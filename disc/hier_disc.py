import tensorflow as tf
import numpy as np
import os
import time
import datetime
import random
from six.moves import xrange  # pylint: disable=redefined-builtin
from disc.hier_rnn_model import Hier_rnn_model
import utils.data_utils as data_utils
import utils.conf as conf

from tensorflow.python.platform import gfile
import sys
from tqdm import tqdm

sys.path.append("../utils")



def hier_read_data(config, query_path, answer_path, gen_path, rev_vocab, sent_path):
    query_set = [[] for _ in config.buckets]
    answer_set = [[] for _ in config.buckets]
    gen_set = [[] for _ in config.buckets]

    with gfile.GFile(query_path, mode="r") as query_file:
        with gfile.GFile(answer_path, mode="r") as answer_file:
            with gfile.GFile(gen_path, mode="r") as gen_file:
                with gfile.GFile(sent_path, mode="w") as sent_file:
                    query, answer, gen = query_file.readline(), answer_file.readline(), gen_file.readline()
                    counter = 0
                    while query and answer and gen:
                        counter += 1
                        if counter % 100000 == 0:
                            print("  reading disc_data line %d" % counter)
                        query = [int(id) for id in query.strip().split()]
                        answer = [int(id) for id in answer.strip().split()]
                        gen = [int(id) for id in gen.strip().split()]

                        sent_file.write(
                            "Query: " + " ".join([str(rev_vocab[id]) for id in query]) + "\n")
                        sent_file.write(
                            "Answer: " + " ".join([str(rev_vocab[id]) for id in answer]) + "\n")
                        sent_file.write(
                            "Fake: " + " ".join([str(rev_vocab[id]) for id in gen]) + "\n")

                        # group dialogues into different buckets based on their length
                        # padding sentences
                        for i, (query_size, answer_size) in enumerate(config.buckets):
                            if 3 < len(query) <= query_size and 3 < len(answer) <= answer_size and 3 < len(
                                    gen) <= answer_size:
                                query = query[:query_size] + [data_utils.PAD_ID] * (
                                    query_size - len(query) if query_size > len(query) else 0)
                                query_set[i].append(query)
                                answer = answer[:answer_size] + [data_utils.PAD_ID] * (
                                    answer_size - len(answer) if answer_size > len(answer) else 0)
                                answer_set[i].append(answer)
                                gen = gen[:answer_size] + [data_utils.PAD_ID] * (
                                    answer_size - len(gen) if answer_size > len(gen) else 0)
                                gen_set[i].append(gen)
                                break
                        query, answer, gen = query_file.readline(), answer_file.readline(), gen_file.readline()

    return query_set, answer_set, gen_set


def hier_get_batch(config, max_set, query_set, answer_set, gen_set):
    batch_size = config.batch_size
    if batch_size % 2 == 1:
        return IOError("Error")
    train_query = []
    train_answer = []
    train_labels = []
    half_size = int(batch_size / 2)
    # half human-generated, half machine-generated
    for _ in range(half_size):
        index = random.randint(0, max_set)
        train_query.append(query_set[index])
        train_answer.append(answer_set[index])
        train_labels.append(1)
        train_query.append(query_set[index])
        train_answer.append(gen_set[index])
        train_labels.append(0)
    return train_query, train_answer, train_labels


def create_model(sess, config, name_scope, word2id, initializer=None):
    with tf.variable_scope(name_or_scope=name_scope, initializer=initializer):
        model = Hier_rnn_model(config=config, name_scope=name_scope)
        # disc_ckpt_dir = os.path.abspath(os.path.join(config.model_dir, 'disc_model', "checkpoints"))
        # disc_ckpt_dir = os.path.abspath(os.path.join(config.train_dir, "checkpoints_ent-{}".format(
        # config.ent_weight, )))
        if not config.adv:
            disc_ckpt_dir = os.path.abspath(os.path.join(config.model_dir, 'disc_model', "checkpoints"))
        else:
            disc_ckpt_dir = os.path.abspath(os.path.join(config.model_dir, 'disc_model',
                                                         "data-{}_pre_embed-{}_exp-{}".format(
                                                             config.data_id,
                                                             config.pre_embed,
                                                            config.exp_id)))
        if config.continue_train:
            disc_ckpt_dir = os.path.abspath(
                os.path.join(config.model_dir, 'disc_model', "data-{}_pre_embed-{}_ent-{}_exp-{}_teacher-{}".format(
                    config.data_id,
                    config.pre_embed,
                    config.ent_weight,
                    config.exp_id,
                    config.teacher_forcing)))
        ckpt = tf.train.get_checkpoint_state(disc_ckpt_dir)
        if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
            print("Reading Hier Disc model parameters from %s" % ckpt.model_checkpoint_path)
            model.saver.restore(sess, ckpt.model_checkpoint_path)
            # load_embeddings_disc(sess, name_scope, word2id, config.word_embedding, True)
        else:
            print("Created Hier Disc model with fresh parameters.")
            disc_global_variables = [gv for gv in tf.global_variables() if name_scope in gv.name]
            sess.run(tf.variables_initializer(disc_global_variables))
            if config.pre_embed:
                load_embeddings_disc(sess, name_scope, word2id, config.word_embedding, False)
        return model


def load_embeddings_disc(sess, name_scope, word2id, embedding_size, pre_trained=False):
    """ Initialize embeddings with pre-trained word2vec vectors
            Will modify the embedding weights of the current loaded model
            Uses the GoogleNews pre-trained values (path hardcoded)
            """
    # Fetch embedding variables from model
    # with tf.variable_scope(name_scope+"/embedding_rnn_seq2seq/rnn/embedding_wrapper", reuse=True):
    embedding_path = '/home/zli1/dialogue-gan/data/embedding/GoogleNews-vectors-negative300.bin'
    variables = tf.get_collection_ref(tf.GraphKeys.TRAINABLE_VARIABLES)
    with tf.variable_scope("Hier_RNN_encoder/rnn/embedding_wrapper", reuse=True):
        em_in = tf.get_variable("embedding")
        # variables.remove(em_in)

    if pre_trained:
        return

    # New model, we load the pre-trained word2vec data and initialize embeddings
    embeddings_path = embedding_path
    embeddings_format = os.path.splitext(embeddings_path)[1][1:]
    print("Loading pre-trained word embeddings from %s " % embeddings_path)
    with open(embeddings_path, "rb") as f:
        header = f.readline()
        vocab_size, vector_size = map(int, header.split())
        binary_len = np.dtype('float32').itemsize * vector_size
        initW = np.random.uniform(-0.25, 0.25, (len(word2id), vector_size))
        initW_flag = [0] * len(word2id)
        for line in tqdm(range(vocab_size)):
            word = []
            while True:
                ch = f.read(1)
                if ch == b' ':
                    word = b''.join(word).decode('utf-8')
                    break
                if ch != b'\n':
                    word.append(ch)
            if word in word2id:
                if embeddings_format == 'bin':
                    vector = np.fromstring(f.read(binary_len), dtype='float32')
                elif embeddings_format == 'vec':
                    vector = np.fromstring(f.readline(), sep=' ', dtype='float32')
                else:
                    raise Exception("Unkown format for embeddings: %s " % embeddings_format)
                initW[word2id[word]] = vector
                initW_flag[word2id[word]] = 1
            else:
                if embeddings_format == 'bin':
                    f.read(binary_len)
                elif embeddings_format == 'vec':
                    f.readline()
                else:
                    raise Exception("Unkown format for embeddings: %s " % embeddings_format)

    all_num = 0
    for x_item in word2id:
        if initW_flag[word2id[x_item]] == 0:
            # print(x_item)
            all_num += 1
    print("Missing words number: %d" % all_num)
    # PCA Decomposition to reduce word2vec dimensionality
    if embedding_size < vector_size:
        U, s, Vt = np.linalg.svd(initW, full_matrices=False)
        S = np.zeros((vector_size, vector_size), dtype=complex)
        S[:vector_size, :vector_size] = np.diag(s)
        initW = np.dot(U[:, :embedding_size], S[:embedding_size, :embedding_size])

    # Initialize input and output embeddings
    sess.run(em_in.assign(initW))


def prepare_data(config):
    train_path = os.path.join('./data', config.data_id, "train")
    test_path = os.path.join('./data', config.data_id, "test")
    dev_path = os.path.join('./data', config.data_id, "dev")
    voc_file_path = [train_path + ".answer", train_path + ".query", test_path + ".answer", test_path + ".query",
                     dev_path + ".answer", dev_path + ".query"]
    vocab_path = os.path.join(config.data_dir, "vocab%d.all" % config.vocab_size)
    data_utils.create_vocabulary(vocab_path, voc_file_path, config.vocab_size)
    vocab, rev_vocab = data_utils.initialize_vocabulary(vocab_path)

    print("Preparing train disc_data in %s" % config.data_dir)
    train_query_path, train_answer_path, train_gen_path, dev_query_path, dev_answer_path, dev_gen_path = \
        data_utils.hier_prepare_disc_data(config.data_dir, vocab, config.vocab_size)
    query_train, answer_train, gen_train = hier_read_data(config, train_query_path, train_answer_path, train_gen_path,
                                                          rev_vocab, os.path.join(config.data_dir, "train.txt"))
    query_dev, answer_dev, gen_dev = hier_read_data(config, dev_query_path, dev_answer_path, dev_gen_path,
                                                    rev_vocab, os.path.join(config.data_dir, "test.txt"))
    return query_train, answer_train, gen_train, query_dev, answer_dev, gen_dev, vocab, rev_vocab


# TODO: this function is used to reuse the dataset (OpenSubTitle) from Jiwei Li's GAN paper
def prepare_data_OpenSub(config):
    vocab_path = os.path.join(config.data_dir, "movie_%d" % config.vocab_size)
    vocab, rev_vocab = data_utils.initialize_vocabulary(vocab_path)

    train_query_path, train_answer_path, train_gen_path, dev_query_path, dev_answer_path, dev_gen_path, \
    test_query_path, test_answer_path, test_gen_path = data_utils.hier_prepare_disc_data_OpenSub(config.data_dir)

    query_train, answer_train, gen_train = hier_read_data(config, train_query_path, train_answer_path, train_gen_path,
                                                          rev_vocab, os.path.join(config.data_dir, "train.txt"))
    query_dev, answer_dev, gen_dev = hier_read_data(config, dev_query_path, dev_answer_path, dev_gen_path, rev_vocab,
                                                    os.path.join(config.data_dir, "dev.txt"))
    query_test, answer_test, gen_test = hier_read_data(config, test_query_path, test_answer_path, test_gen_path,
                                                       rev_vocab, os.path.join(config.data_dir, "test.txt"))

    return query_train, answer_train, gen_train, query_dev, answer_dev, gen_dev, query_test, answer_test, gen_test


def softmax(x):
    prob = np.exp(x) / np.sum(np.exp(x), axis=0)
    return prob


def hier_train(config_disc, config_evl):
    print("begin training")

    with tf.Session() as session:
        print("prepare_data")
        query_set, answer_set, gen_set, query_dev, answer_dev, gen_dev, vocab, rev_vocab = prepare_data(config_disc)

        for set in query_set:
            print("query set length: ", len(set))
        model = create_model(session, config_disc, name_scope=config_disc.name_model, word2id=vocab)

        step_time, loss = 0.0, 0.0
        current_step = 0
        step_loss_summary = tf.Summary()
        disc_writer = tf.summary.FileWriter(config_disc.tensorboard_dir, session.graph)

        while True:
            bucket_id = current_step % len(config_disc.buckets)
            start_time = time.time()
            b_query, b_answer, b_gen = query_set[bucket_id], answer_set[bucket_id], gen_set[bucket_id]
            data_reader = data_utils.Data_Reader(b_query, b_answer, b_gen, train_ratio=config_disc.train_ratio,
                                                 flag="train")
            batch_number = data_reader.get_batch_num(config_disc.batch_size)
            epoch_loss = 0

            # print("start testing: ")
            if current_step % 10 == 0:
                adversuc_avg, loss_list = AdverSuc_test(session, model, config_evl, query_dev, answer_dev, gen_dev)
                print("Epoch_step %d, the testing AdverSuc is: %.4f" % (current_step, adversuc_avg))
                print(loss_list)

            for batch_index in range(batch_number):
                train_query, train_answer, train_labels = data_reader.generate_training_batch(config_disc.batch_size)
                train_query = np.transpose(train_query)
                train_answer = np.transpose(train_answer)

                feed_dict = {}
                for i in xrange(config_disc.buckets[bucket_id][0]):
                    feed_dict[model.query[i].name] = train_query[i]
                for i in xrange(config_disc.buckets[bucket_id][1]):
                    feed_dict[model.answer[i].name] = train_answer[i]
                feed_dict[model.target.name] = train_labels

                fetches = [model.b_train_op[bucket_id], model.b_logits[bucket_id], model.b_loss[bucket_id],
                           model.target, model.learning_rate_decay_op]
                train_op, logits, step_loss, target, _ = session.run(fetches, feed_dict)
                epoch_loss += step_loss / batch_number

                # softmax operation
                logits = np.transpose(softmax(np.transpose(logits)))

                reward = 0.0
                # here, logits has two dimensions, which correspond to two labels (false and true)
                for logit, label in zip(logits, train_labels):
                    reward += logit[label]
                reward = reward / len(train_labels)
                if batch_index == 0:
                    print("epoch_step: %d, batch_num: %d, bucket_id: %d, reward: %.4f , step_loss: %.4f " % (
                        current_step, batch_index, bucket_id, reward, step_loss))
            print("Epoch_step: %d, epoch_loss: %.4f" % (current_step, epoch_loss))

            step_time += (time.time() - start_time) / config_disc.steps_per_checkpoint
            loss += epoch_loss / config_disc.steps_per_checkpoint
            current_step += 1

            if current_step % config_disc.steps_per_checkpoint == 0:
                disc_loss_value = step_loss_summary.value.add()
                disc_loss_value.tag = config_disc.name_loss
                disc_loss_value.simple_value = float(loss)

                disc_writer.add_summary(step_loss_summary, int(session.run(model.global_step)))

                if current_step % (config_disc.steps_per_checkpoint * 2) == 0:
                    print("current_step: %d, save_model" % current_step)
                    disc_ckpt_dir = os.path.abspath(os.path.join(config_disc.model_dir, 'disc_model',
                                                         "data-{}_pre_embed-{}_exp-{}".format(
                                                             config_disc.data_id,
                                                             config_disc.pre_embed,
                                                             config_disc.exp_id)))
                    if not os.path.exists(disc_ckpt_dir):
                        os.makedirs(disc_ckpt_dir)
                    disc_model_path = os.path.join(disc_ckpt_dir, "disc.model")
                    model.saver.save(session, disc_model_path, global_step=model.global_step)

                step_time, loss = 0.0, 0.0
                sys.stdout.flush()


def hier_test(session, model, config_evl, query_test, answer_test, gen_test, test_ratio):
    bucket_num = len(config_evl.buckets)
    batch_size = config_evl.batch_size
    score_all = 0
    num_all = 0
    loss = 0
    for bucket_id in range(bucket_num):
        data_reader = data_utils.Data_Reader(query_test[bucket_id], answer_test[bucket_id], gen_test[bucket_id],
                                             test_ratio=test_ratio, flag="test")
        batch_number = data_reader.get_batch_num(batch_size)
        for batch_id in range(batch_number):
            train_query, train_answer, train_labels = data_reader.generate_testing_batch(batch_size)
            train_query = np.transpose(train_query)
            train_answer = np.transpose(train_answer)

            feed_dict = {}
            for i in xrange(config_evl.buckets[bucket_id][0]):
                feed_dict[model.query[i].name] = train_query[i]
            for i in xrange(config_evl.buckets[bucket_id][1]):
                feed_dict[model.answer[i].name] = train_answer[i]
            feed_dict[model.target.name] = train_labels

            fetches = [model.b_logits[bucket_id], model.b_loss[bucket_id]]
            logits, step_loss = session.run(fetches, feed_dict)
            loss += step_loss / batch_number
            for logit, label in zip(logits, train_labels):
                num_all += 1
                if logit[label] - logit[1 - label] >= 0:
                    score_all += 1
    return score_all * 1.00 / num_all, loss / bucket_num


def AdverSuc_test(session, model, config_evl, query_test, answer_test, gen_test):
    adversuc_1, loss_1 = hier_test(session, model, config_evl, query_test, answer_test, gen_test, 0.0)
    adversuc_2, loss_2 = hier_test(session, model, config_evl, query_test, answer_test, gen_test, 0.5)
    adversuc_3, loss_3 = hier_test(session, model, config_evl, query_test, answer_test, gen_test, 1.0)
    adversuc_1 = np.abs(adversuc_1 - 0.5)
    adversuc_2 = np.abs(adversuc_2 - 1.0)
    adversuc_3 = np.abs(adversuc_3 - 0.5)
    return (adversuc_1 + adversuc_2 + adversuc_3) / 3, [(adversuc_1, loss_1)] + [(adversuc_2, loss_2)] + [
        (adversuc_3, loss_3)]


def main(_):
    disc_config = conf.disc_config
    hier_train(disc_config, disc_config)


if __name__ == "__main__":
    tf.app.run()
